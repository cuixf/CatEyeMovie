package com.yuhj.cateyemovie.fragment;

import com.yuhj.cateyemovie.R;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * @name MovieFragment
 * @Descripation 社区模块的主界面<br>
 * @author 禹慧军
 * @date 2014-10-30
 * @version 1.0
 */
public class HomeFragment extends Fragment {

	public HomeFragment() {
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view =inflater.inflate(R.layout.home_fragment, container,false);
		return view;
	}

}
