package com.yuhj.cateyemovie.fragment;

import java.lang.reflect.Field;
import java.util.ArrayList;
import org.apache.http.conn.scheme.PlainSocketFactory;
import com.baidu.location.BDLocation;
import com.baidu.location.BDLocationListener;
import com.baidu.location.LocationClient;
import com.baidu.location.LocationClientOption;
import com.baidu.location.LocationClientOption.LocationMode;
import com.baidu.mapapi.SDKInitializer;
import com.yuhj.cateyemovie.R;
import com.yuhj.cateyemovie.activitys.CityListActivity;
import com.yuhj.cateyemovie.activitys.MainActivity;
import com.yuhj.cateyemovie.activitys.MovieDetailActivity;
import com.yuhj.cateyemovie.adapter.SelectPlayingPageAdapter;

import android.R.integer;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * @name MovieFragment
 * @Descripation 电影模块的主界面<br>
 * @author 禹慧军
 * @date 2014-10-30
 * @version 1.0
 */
public class MovieFragment extends Fragment implements OnClickListener,
		OnPageChangeListener {
	private ViewPager viewPager;
	private Button playingButton, willPlayButton;
	private ImageButton select_cityButton;
	private ImageButton searchMovie;
	private SelectPlayingPageAdapter pageAdapter;
	private ArrayList<Fragment> fragments;
	private TextView txt_city;
	private RelativeLayout parmarm;
	private FrameLayout.LayoutParams params;
	private TextView imgchange;
	private int offset;
	
	public MovieFragment() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);

	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View view = inflater.inflate(R.layout.movie_fragment, container, false);
		initView(view);
		fragments =new ArrayList<Fragment>();
		fragments.add(new PalyingFragment());
		fragments.add(new WillPlayingFragment());
		pageAdapter = new SelectPlayingPageAdapter(getChildFragmentManager(),
				fragments);
		viewPager.setAdapter(pageAdapter);
		return view;
	}

	private void initView(View view) {
		viewPager = (ViewPager) view.findViewById(R.id.select_movie_viewpager);
		viewPager.setOnPageChangeListener(this);
		playingButton = (Button) view.findViewById(R.id.palying);
		playingButton.setOnClickListener(this);
		willPlayButton = (Button) view.findViewById(R.id.will_paly);
		txt_city=(TextView) view.findViewById(R.id.txt_main_city);
		Bundle bundle =getArguments();
		txt_city.setText(bundle.getString("city","北京"));
		willPlayButton.setOnClickListener(this);
		searchMovie = (ImageButton) view.findViewById(R.id.search_movie);
		searchMovie.setOnClickListener(this);
		select_cityButton = (ImageButton) view.findViewById(R.id.select_city);
		select_cityButton.setOnClickListener(this);
		txt_city.setOnClickListener(this);
		imgchange=(TextView) view.findViewById(R.id.cursor);
	}

	@Override
	public void onClick(View view) {
		if (view==select_cityButton||view==txt_city) {
			startActivity(new Intent(getActivity(),CityListActivity.class));
			getActivity().finish();
		}else if (view==playingButton) {
			viewPager.setCurrentItem(0);
		}else if (view==willPlayButton) {
			viewPager.setCurrentItem(1);
		}else if (view==searchMovie) {
			startActivity(new Intent(getActivity(),MovieDetailActivity.class));
		}

	}

	@Override
	public void onPageScrollStateChanged(int arg0) {
		
	}

	private boolean flag=true;
	@Override
	public void onPageScrolled(int arg0, float arg1, int arg2) {
		if (arg0==0) {
			if (arg1<0.99) {
				imgchange.setX(imgchange.getWidth()*arg1);
			}
		}
		
	}

	@Override
	public void onPageSelected(int arg0) {	
	}


	
	@Override
	public void onDetach() {
		try {
			Field childFragmentManager = Fragment.class
					.getDeclaredField("mChildFragmentManager");
			childFragmentManager.setAccessible(true);
			childFragmentManager.set(this, null);
			

		} catch (NoSuchFieldException e) {
			// throw new RuntimeException(e);
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// throw new RuntimeException(e);
			e.printStackTrace();
		}
		super.onDetach();
	}
	
}
