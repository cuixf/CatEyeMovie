package com.yuhj.cateyemovie.effect;

import android.support.v4.view.ViewPager.PageTransformer;
import android.view.View;

/**
 * @name InRightUpTransformer
 * @Descripation <br>
	右上角进入
 * @author 禹慧军
 * @date 2014-11-5
 * @version 1.0
 */
public class InRightUpTransformer implements PageTransformer {

	@Override
	public void transformPage(View view, float position) {
		int pageHeight = view.getHeight();
		if (position < -1) {
			view.setAlpha(1);
			view.setTranslationY(0);
		} else if (position <= 0) {
			view.setTranslationY(pageHeight * -position);
			view.setAlpha(1 + position);

			// ViewHelper.setTranslationY(view, pageHeight * -position);
			// ViewHelper.setAlpha(view, 1 + position);
		} else if (position <= 1) {
			view.setTranslationY(view.getHeight() * -position);
			view.setAlpha(1 - position);

			// ViewHelper.setTranslationY(view, pageHeight * -position);
			// ViewHelper.setAlpha(view, 1 - position);
		} else {
			view.setTranslationY(0);
			view.setAlpha(1);
		}
	}

}
