package com.yuhj.cateyemovie.bean;

import android.R.integer;

/**
 * @name PlaingMovieList
 * @Descripation 正在上映的电影的List列表<br>
  
 * @author 禹慧军
 * @date 2014-11-1
 * @version 1.0
 */
public class PlayingMovieList {
	
	/**
	 * 题目
	 */
	private String name;
	
	private long wish;

	
	public long getWish() {
		return wish;
	}

	public void setWish(long wish) {
		this.wish = wish;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImg() {
		return img;
	}

	public void setImg(String img) {
		this.img = img;
	}

	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}

	public boolean isNew() {
		return isNew;
	}

	public void setNew(boolean isNew) {
		this.isNew = isNew;
	}

	public String getSummmary() {
		return summmary;
	}

	public void setSummmary(String summmary) {
		this.summmary = summmary;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}
	private int id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	/**
	 * 剧照
	 */
	private String img;
	
	/**
	 * 类型
	 */
	private String tag;
	
	/**
	 * 是否新电影
	 */
	private boolean isNew;
	
	/**
	 * 电影摘要
	 */
	private String summmary;
	
	/**
	 * 实体
	 */
	private String content;
	
	
	/**
	 * 评分
	 */
	private String level;
	
	public PlayingMovieList() {
		// TODO Auto-generated constructor stub
	}

}
