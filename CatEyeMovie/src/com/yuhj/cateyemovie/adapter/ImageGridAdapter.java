package com.yuhj.cateyemovie.adapter;

import java.util.ArrayList;

import com.yuhj.cateyemovie.R;
import com.yuhj.cateyemovie.utils.ImageCache;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.util.LruCache;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

public class ImageGridAdapter extends BaseAdapter {
	private Context context;
	private ArrayList<String> image;
	private LruCache<String,Bitmap> lruCache;
	public ImageGridAdapter(Context context,ArrayList<String> images) {
		this.context=context;
		this.image= images;
		this.lruCache =ImageCache.GetLruCache(context);
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return image.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return image.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = null;
		if (convertView==null) {
			view = LayoutInflater.from(context).inflate(R.layout.imageview, null);
		}else {
			view = convertView;
		}
		ImageView imageView =(ImageView) view.findViewById(R.id.imageview);
		imageView.setTag(image.get(position));
		imageView.setImageResource(R.drawable.bg_default_cat_gray);
		new ImageCache(context, lruCache, imageView, image.get(position), "CatEyeMovie", 100, 100);
		return view;
	}

}
